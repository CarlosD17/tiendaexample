from django.conf.urls import url
from .views import *

app_name='productos'
urlpatterns = [
    url(r'^$', compras_view, name="compras_view"),
    url(r'^productos$', index_view, name="index_view"),
    url(r'^categorias$', Categorias.as_view(), name="categorias"),
    url(r'^nuevoProducto$', nuevo_producto, name="nuevo_producto"),
    url(r'^editarProducto/(?P<idProducto>.*)$', editar_producto, name="editar_producto"),
    url(r'^eliminarProducto/(?P<idProducto>.*)$', eliminar_producto, name="eliminar_producto"),
    url(r'^nuevaCategoria$', nueva_categoria, name="nueva_categoria"),
    url(r'^editarCategoria/(?P<idCategoria>.*)$', editar_categoria, name="editar_categoria"),
    url(r'^eliminarCategoria/(?P<idCategoria>.*)$', eliminar_categoria, name="eliminar_categoria"),
    url(r'^compras$', compras_view, name="compras_view"),
    url(r'^realizar_compra$', realizar_compra, name="realizar_compra"),
]
