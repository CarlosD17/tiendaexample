# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse, JsonResponse, HttpRequest
from django.shortcuts import render
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from AR_server import models
import json
from AR_Django.settings import BASE_DIR
from django.db.models import Q
from datetime import datetime
import requests
import os
from django.core.files.base import ContentFile
from django.contrib.staticfiles.templatetags.staticfiles import static
# Create your views here.

# Teacher_view()
# @author  Carlos Daniel Estrada
 # renderiza la vista de docente
 # @return  html de la vista de docente


def Teacher_view(request):
    return render(request, "subirDocumentos.html");


# get_friends()
#Recibe el usuario de la sesiòn y busca en la tabla de amigos
# los que coincidan con el usuario y obtiene el id y nombre de sus amigos
# @author  Carlos Daniel Estrada
# @param iduser = usuario de la sesion
# @return json con los datos de los amigos


def get_friends(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    dictionary={}
    personas=[]
    solicitudes = models.Amigos.objects.filter(user_receptor=data["iduser"])
    for datos in solicitudes:
        datosUser = models.User.objects.filter(pk=datos.user_emisor).first()
        personas.append([datosUser.id, datosUser.username])

    if len(personas) > 0:
        dictionary["status"] = "OK"
    else:
        dictionary["status"] = "No tiene amigos"
    dictionary["amigos"]=personas
    return JsonResponse(dictionary)


# get_friend_request()
#Recibe el usuario de la sesiòn y busca en la tabla de solicitudes de amistad
# los que coincidan con el usuario y obtiene el id y nombre de la persona
# @author  Carlos Daniel Estrada
# @param iduser = usuario de la sesion
# @return json con los datos de las solicitudes de amigos



def get_friend_request(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    dictionary={}
    personas=[]
    solicitudes = models.SolicitudAmistad.objects.filter(estatus="Pendiente",user_receptor=data["iduser"])
    for datos in solicitudes:
        datosUser = models.User.objects.filter(pk=datos.user_emisor).first()
        personas.append([datosUser.id, datosUser.username])

    if len(personas) > 0:
        dictionary["status"] = "OK"
    else:
        dictionary["status"] = "No tiene amigos"
    dictionary["solicitud_amigos"]=personas
    return JsonResponse(dictionary)


# accept_request()
# Recibe los dos usuarios y busca una solicitude de amistad y si la encuentra
# la acepta y crea dos reistros de amigos
# @author  Carlos Daniel Estrada
# @param idUserIssuing = usuario emisor
# @param idUserReceiver = usuario receptor
# @return texto con el estatus de la operacion


def accept_request(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    idUserIssuing = data["idUserIssuing"]
    idUserReceiver = data["idUserReceiver"]
    status = "Error"
    solicitud = models.SolicitudAmistad.objects.filter(user_receptor=idUserReceiver, user_emisor=idUserIssuing).first()
    if solicitud:
        solicitud.estatus = "Aceptada"
        solicitud.save()
        check_request = models.Amigos.objects.filter(user_receptor=idUserReceiver, user_emisor=idUserIssuing).first()
        if check_request is not None:
            status = "Ya existe Amistad"
        else:
            add_friend_issuing= models.Amigos(user_receptor=idUserReceiver, user_emisor=idUserIssuing, fecha=datetime.now())
            add_friend_receiver= models.Amigos(user_receptor=idUserIssuing, user_emisor=idUserReceiver, fecha=datetime.now())
            add_friend_issuing.save()
            add_friend_receiver.save()
            status = "Aceptada"
    else:
        status = "No existe solicitud"

    return HttpResponse(status)


# reject_request()
# Recibe los dos usuarios y busca una solicitude de amistad y si la encuentra
# la rechaza
# @author  Carlos Daniel Estrada
# @param idUserIssuing = usuario emisor
# @param idUserReceiver = usuario receptor
# @return texto con el estatus de la operacion


def reject_request(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    idUserIssuing = data["idUserIssuing"]
    idUserReceiver = data["idUserReceiver"]
    status = ""
    solicitud = models.SolicitudAmistad.objects.filter(user_receptor=idUserReceiver, user_emisor=idUserIssuing).first()
    if solicitud:
        solicitud.estatus = "Rechazada"
        solicitud.save()
        status = "Rechazada"
    else:
        status = "No existe solicitud"

    return HttpResponse(status)



# delete_friend()
# Recibe los dos usuarios y busca los registros de amigos y los elimina
# @author  Carlos Daniel Estrada
# @param idUserIssuing = usuario emisor
# @param idUserReceiver = usuario receptor
# @return texto con el estatus de la operacion



def delete_friend(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    idUserIssuing = data["idUserIssuing"]
    idUserReceiver = data["idUserReceiver"]
    status=""
    request_receiver = models.Amigos.objects.filter(user_receptor=idUserReceiver, user_emisor=idUserIssuing).first()
    request_issuing = models.Amigos.objects.filter(user_receptor=idUserIssuing, user_emisor=idUserReceiver).first()
    if request_receiver and request_issuing:
        try:
            request_receiver.delete()
            request_issuing.delete()
            solicitud_issuing = models.SolicitudAmistad.objects.filter(user_receptor=idUserReceiver, user_emisor=idUserIssuing).first()
            if solicitud_issuing:
                solicitud.estatus = "Eliminado"
                solicitud.save()
            else:
                solicitud_receiver = models.SolicitudAmistad.objects.filter(user_receptor=idUserIssuing, user_emisor=idUserReceiver).first()
                solicitud.estatus = "Eliminado"
                solicitud.save()
            status = "Eliminado"
        except Exception as e:
            status = "No se puedo eliminar"
    else:
        status = "No existe solicitud"

    return HttpResponse(status)


# send_friend_request()
# Recibe los dos usuarios y busca los registros de solicitudes de amistad y si
# no existen crea las solicitud
# @author  Carlos Daniel Estrada
# @param idUserIssuing = usuario emisor
# @param idUserReceiver = usuario receptor
# @return Json con el texto con el estatus de la operacion


def send_friend_request(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    idUserIssuing = data["idUserIssuing"]
    idUserReceiver = data["idUserReceiver"]
    status = ""
    dictionary={}
    request_receiver = models.SolicitudAmistad.objects.filter(user_receptor=idUserReceiver, user_emisor=idUserIssuing).first()
    request_issuing = models.SolicitudAmistad.objects.filter(user_receptor=idUserIssuing, user_emisor=idUserReceiver).first()
    if request_receiver or request_issuing:
        status = "Ya existe la solicitud"
    else:
        new_request = models.SolicitudAmistad(user_receptor=idUserReceiver, user_emisor=idUserIssuing)
        new_request.save()
        status ="Solicitud enviada"
    dictionary["status"] =status
    return JsonResponse(dictionary)


# get_profile_information()
# Recibe un IdUsurio y busca su informacio personal
# @author  Carlos Daniel Estrada
# @param idUser = id del usuario
# @return Json con el texto con el estatus de la operacion


def get_profile_information(idUser):
    dictionary={}
    datosUser = models.User.objects.filter(pk=idUser).first()
    if datosUser:
        additional_information_user = models.datosUser.objects.filter(user=datosUser.pk).first()
        dictionary["userData"] = [datosUser.pk, datosUser.email,datosUser.first_name,datosUser.last_name,additional_information_user.img_url,additional_information_user.plan]
        dictionary["status"] = "OK"
    else:
        dictionary["status"] = "NO"

    return JsonResponse(dictionary)


# validate_user()
# Recibe los datos y comprueba si esta registrado en le portal de la facultad de informatica
# @author  Carlos Daniel Estrada
# @param exp = expediente del usuario
# @param password = password del usuario
# @return texto con el estatus de la operacion


def validate_user(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    expediente = data["exp"]
    password = data["password"]
    r = requests.post('https://portalinformatica.uaq.mx/portalInformatica/portal-informatica-api-iniciar-sesion', data={'expediente':expediente, 'password':password})
    r =  r.json()
    dictionary ={}
    if r["alumno"] is not None:
        dictionary["status"] = "OK"
        dictionary["dataA"] = r["alumno"]
    else:
        dictionary["status"] = "No es alumno de la facultad"
    return JsonResponse(dictionary)


# create_user()
# Recibe los datos y registra un nuevo usuario
# @author  Carlos Daniel Estrada
# @param exp = expediente del usuario
# @param first_name = nombre del usuario
# @param last_name = apellidos del usuario
# @param plan = plan de estudio del usuario
# @param username = nombre de usuario
# @param email = email del usuario
# @return Json con el texto con el estatus de la operacion


def create_user(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)

    exp = data["exp"]
    password = data["exp"]+"."
    first_name = data["first_name"]
    last_name = data["last_name"]
    plan = data["plan"]
    username = data["username"]
    email = data["email"]
    status = ""
    dictionary={}
    _user = User.objects.filter(pk=exp).first()
    if _user is None:
        try:
            user = User.objects.create_user(id=exp, password=password, first_name=first_name, last_name=last_name, username=username, email=email)
            user.save()
            _rol = models.Roles.objects.filter(pk=1).first()
            add_data_user = models.datosUser(user=user, rol=_rol, plan=plan)
            add_data_user.save()
            status = "OK"
        except Exception as e:
            status = "No se puedo registrar usuario"

    else:
        status = "Ya existe un usuario con ese expediente"
    dictionary["status"] = status
    return JsonResponse(dictionary)


# authenticate_login()
# Recibe los datos y comprueba si el usuario esta registrado en el sistema
# @author  Carlos Daniel Estrada
# @param user = nombre de usuario
# @param password = password de usuario
# @return Json con el texto con el estatus de la operacion


def authenticate_login(request):
    dictionary={}
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    user =  data["user"]
    password = data["password"]
    autentificar = authenticate(username=user, password=password)
    if autentificar is not None:
        dictionary["estatus"] = "OK"
        dictionary["dataUser"]= autentificar.id
        rol = models.datosUser.objects.filter(user=autentificar.id).first()
        dictionary["rolUser"]= rol.rol.rol
    else:
        dictionary["estatus"] = "Usuario o contraseña incorrecta"
    return JsonResponse(dictionary)


# authenticate_login()
# Recibe los datos y comprueba si el usuario esta registrado en el sistema
# @author  Carlos Daniel Estrada
# @param user = nombre de usuario
# @param password = password de usuario
# @return Json con el texto con el estatus de la operacion


def get_user_search(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    data_search = data["dataS"]
    persons = []
    dictionary={}
    data = User.objects.filter(Q(id__contains=data_search) | Q(username__contains=data_search) | Q(first_name__contains=data_search) | Q(last_name__contains=data_search))
    for values in data:
        persons.append([values.id, values.username])

    dictionary["persons"] = persons
    return JsonResponse(dictionary)


# get_friends_search()
# recibe los datos datos y busca en usuarios nombre, espediente, apellido,
# username que se parezca al texto de la busqueda
# @author  Carlos Daniel Estrada
# @param dataS =  texto de la busqueda
# @param iduser = id del usuario
# @return Json con los arreglos de personas encontradas


def get_friends_search(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    data_search = data["dataS"]
    iduser = data["iduser"]
    dictionary={}
    personas=[]
    data = User.objects.filter(Q(id__contains=data_search) | Q(username__contains=data_search) | Q(first_name__contains=data_search) | Q(last_name__contains=data_search))
    for values in data:
        try:
            solicitudes = models.Amigos.objects.filter(user_receptor=iduser, user_emisor=values.pk).first()
            datosUser = models.User.objects.filter(pk=solicitudes.user_emisor).first()
            personas.append([datosUser.id, datosUser.username])
        except Exception as e:
            print e
    dictionary["persons"] = personas
    print dictionary
    return JsonResponse(dictionary)




def get_information_teacher(request):
    # body_unicode = request.body.decode('utf-8')
    # data = json.loads(body_unicode)
    idUser = 1
    data = get_profile_information(idUser)
    if data["status"] != "NO" :
        aditional_data_teacher = "obtener_datos"
    else:
        print "No existe"
    return HttpResponse("Comprobar los modelos")



# get_friends_search()
# recibe los datos datos y busca en usuarios nombre, espediente, apellido,
# username que se parezca al texto de la busqueda
# @author  Carlos Daniel Estrada
# @param dataS =  texto de la busqueda
# @param iduser = id del usuario
# @return Json con los arreglos de personas encontradas


def get_profile_data(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    idUser = data["idUser"]
    data = get_profile_information(idUser)
    return HttpResponse(data)

def get_information_classroom(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    classroom = models.Aula.objects.filter(nombre=data["idClassroom"]).first()
    idClassroom = classroom.pk
    dayOfWeek = data["dayOfWeek"]
    print dayOfWeek
    dictionary = {}
    infoClassroom = []
    dataSchedules = models.Horario.objects.filter(aula=idClassroom, dia=dayOfWeek)
    for data in dataSchedules:
        Teacher = models.Docente.objects.filter(pk=data.docente.pk).first()
        aditionalInformation= models.datosUser.objects.filter(user=data.docente.user).first()
        dataTeacher = models.User.objects.filter(pk=int(Teacher.user.pk)).first()
        dataClass = models.Materia.objects.filter(pk=int(data.materia.pk)).first()
        dataClassroom = models.Aula.objects.filter(pk=int(data.aula.pk)).first()
        infoClassroom.append([data.horaInicio+"-"+data.horaFinal, data.dia,dataTeacher.first_name + " "+dataTeacher.last_name, dataClass.nombre, dataClass.carrera, dataClassroom.nombre,aditionalInformation.img_url])
    dictionary["infoClassroom"]=infoClassroom
    dictionary["lengthClass"] = len(infoClassroom)
    print dictionary
    return JsonResponse(dictionary)


def get_classroom(request):
    data=[]
    dictionary={}
    dataClassroom = models.Aula.objects.all()
    for dataClassroomS in dataClassroom:
        data.append([dataClassroomS.id, dataClassroomS.nombre])
    dictionary["dataClassroom"] = data
    return JsonResponse(dictionary)


def get_lessons(request):
    data=[]
    dictionary={}
    dataLessons = models.Materia.objects.all()
    for dataLessonsS in dataLessons:
        data.append([dataLessonsS.id, dataLessonsS.nombre + "   " +dataLessonsS.carrera+"   "+dataLessonsS.clave])
    dictionary["dataLessons"] = data
    return JsonResponse(dictionary)


def get_teachers(request):
    data=[]
    dictionary={}
    dataTeachers = models.User.objects.all()
    for dataTeachersS in dataTeachers:
        try:
                aditionalInformation = models.datosUser.objects.filter(user=dataTeachersS.pk).first()
                if aditionalInformation.rol.rol == "Docente":
                    data.append([dataTeachersS.id, dataTeachersS.first_name+ " " +dataTeachersS.last_name])
        except Exception as e:
            raise
    dictionary["dataTeachers"] = data
    print dictionary
    return JsonResponse(dictionary)


def create_comment_lesson(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    dictionary={}
    idUser = data["idUser"]
    comment = data["comment"]
    idLesson = data["idLesson"]
    try:
        user = models.User.objects.filter(pk=idUser).first()
        comment = models.Comentario(comentario=comment,fecha=datetime.now(),tipoComentario="Clase",estatus="Pendiente",id_clase_docente=idLesson,user=user)
        comment.save()
        dictionary["status"] = "OK"
    except Exception as e:
        dictionary["status"] ="Error al crear comentario"
    return JsonResponse(dictionary)


def create_comment_teacher(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    dictionary={}
    idUser = data["idUser"]
    comment = data["comment"]
    idTeacher = data["idTeacher"]
    try:
        user = models.User.objects.filter(pk=idUser).first()
        comment = models.Comentario(comentario=comment,fecha=datetime.now(),tipoComentario="Docente",estatus="Pendiente",id_clase_docente=idTeacher,user=user)
        comment.save()
        dictionary["status"] = "OK"
    except Exception as e:
        print e
        dictionary["status"] = "Error al crear comentario"
    return JsonResponse(dictionary)


def get_comments_class(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    dictionary={}
    dataComments=[]
    idClass = data["idClass"]
    # idClass=1
    allComments = models.Comentario.objects.filter(id_clase_docente=idClass, tipoComentario="Clase",estatus="Aprobado")
    for comment in allComments:
        dataComments.append([comment.comentario,comment.fecha])
    dictionary["Comments"]=dataComments
    return JsonResponse(dictionary)


def get_comments_teacher(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    dictionary={}
    dataComments=[]
    idTeacher = data["idTeacher"]
    # idClass=1
    allComments = models.Comentario.objects.filter(id_clase_docente=idTeacher, tipoComentario="Docente",estatus="Aprobado")
    for comment in allComments:
        dataComments.append([comment.comentario,comment.fecha])
    dictionary["Comments"]=dataComments
    print dictionary
    return JsonResponse(dictionary)




def get_main_markers(request):
    dictionary={}
    dataMarkers=[]
    main_markers = models.CIL_CIL.objects.values("CIL_primario").distinct()
    for marker in main_markers:
        try:
            dataMarker = models.CampoInformativoLista.objects.filter(pk=int(marker["CIL_primario"])).first()
            dataMarkers.append([dataMarker.pk, dataMarker.titulo])
        except Exception as e:
            raise

    dictionary["dataMarkers"]=dataMarkers
    return JsonResponse(dictionary)


def add_marker(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    dictionary={}
    status=""
    title=data["tittle"]
    text = data["text"]
    idMain_marker = data["idMain_marker"]
    iduser = data["iduser"]
    try:
        point = models.PuntoInteres.objects.filter(user=iduser).first()
        if point is None:
            point = models.PuntoInteres(user=iduser)
            point.save()
        CIC = models.CampoInformativoContenido(titulo=title, texto=text,fechaCreacion=datetime.now(), puntoInteres=point)
        CIC.save()
        main_marker = models.CampoInformativoLista.objects.filter(pk=idMain_marker).first()
        second_marker =  models.CampoInformativoLista.objects.filter(titulo="Usuarios").first()
        CIL = models.CIL_CIL.objects.filter(CIL_primario=main_marker.pk, CIL_secundario=second_marker.pk).first()
        CIC_CIL = models.CIC_CIL(CIC=CIC, CIL=CIL.pk)
        CIC_CIL.save()
        status="Marcador Creado"
    except Exception as e:
        print e
        status="Error al crear marcador"
    dictionary["status"] = status

    return JsonResponse(dictionary)

def delete_marker(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    dictionary={}
    status=""
    idMarker = data["idMarker"]
    try:
        CIC = models.CampoInformativoContenido.objects.filter(pk=idMarker).first()
        CIC.delete()
        status="Marcador Eliminado"
    except Exception as e:
        status="Error al eliminar"

    dictionary["status"] = status
    return JsonResponse(dictionary)


def get_information_main_marker(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    dictionary={}
    status=""
    dataMarker=[]
    idMarker = data["idMain_marker"]
    print idMarker
    CIL = models.CampoInformativoLista.objects.filter(pk=idMarker).first()
    dictionary["tittle"] = CIL.titulo
    dictionary["id"] = CIL.pk
    return JsonResponse(dictionary)


def get_markers_user(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    dictionary={}
    dataMarkers=[]
    status=""
    iduser = data["iduser"]
    print iduser
    datosUser = models.User.objects.filter(pk=iduser).first()
    point = models.PuntoInteres.objects.filter(user=datosUser.pk).first()
    if point is None:
        status = "Sin Marcadores"
    else:
        CIC = models.CampoInformativoContenido.objects.filter(puntoInteres=point)
        for markers in CIC:
            dataMarkers.append([markers.pk, markers.titulo])
            status="OK"
    dictionary["dataMarkers"]= dataMarkers
    dictionary["status"]=status
    print dictionary
    return JsonResponse(dictionary)


def share_marker(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    dictionary={}
    status=""
    idCIC = data["idCIC"]
    idUserIssuing = data["idUserIssuing"]
    idUserReceiver = data["idUserReceiver"]
    share = models.MarcadoresCompartidos.objects.filter(user_emisor=idUserIssuing, user_receptor=idUserReceiver, CIC=idCIC).first()
    print share
    if share is not None:
        status = "Ya se compartio con este usuario"
    else:
        try:
            CIC = models.CampoInformativoContenido.objects.filter(pk=idCIC).first()
            share = models.MarcadoresCompartidos(user_emisor=idUserIssuing, user_receptor=idUserReceiver, CIC=CIC)
            share.save()
            status = "OK"
        except Exception as e:
            print e
            status = "Error al agregar marcador"

    dictionary["status"]=status
    return JsonResponse(dictionary)

def delete_share_marker(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    dictionary={}
    status=""
    idCIC = data["idCIC"]
    idUserIssuing = data["idUserIssuing"]
    idUserReceiver = data["idUserReceiver"]

    try:
        CIC = models.CampoInformativoContenido.objects.filter(pk=idCIC).first()
        share = models.MarcadoresCompartidos.objects.filter(user_emisor=idUserIssuing, user_receptor=idUserReceiver, CIC=CIC)
        share.delete()
        status = "Marcador Eliminado"
    except Exception as e:
        status = "Error al eliminar"

    dictionary["status"]=status
    return JsonResponse(dictionary)


def get_user_shared(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    dictionary={}
    personas=[]
    status=""
    idUserIssuing = data["idUserIssuing"]
    idCIC = data["idCIC"]

    share = models.MarcadoresCompartidos.objects.filter(user_emisor=idUserIssuing, CIC=idCIC)

    for markers in share:
        datosUser = models.User.objects.filter(pk=markers.user_receptor).first()
        personas.append([datosUser.id, datosUser.username])

    dictionary["persons"] = personas
    return JsonResponse(dictionary)


def upload_document(request):
    dictionary={}
    archivo = request.FILES.get('myFile')
    classroom = request.POST.get("classroom")
    document_name=archivo.name
    idTeacher= request.POST.get("teacher")
    name = str(document_name)
    if archivo is not None:
        url = 'static/Documents/'+str(classroom)
        directorio = os.path.join(BASE_DIR,url)
        try:
          os.stat(directorio)
        except:
          os.mkdir(directorio)

        url = directorio+"/"+str(idTeacher)
        directorio = os.path.join(BASE_DIR,url)
        print directorio
        try:
          os.stat(directorio)
        except:
          os.mkdir(directorio)

        filename = os.path.join(directorio,name)
        f = open(filename, 'wb+')
        file_content = ContentFile(archivo.read())
        try:
            for chunk in file_content.chunks():
                f.write(chunk)
            f.close()
            teacher = models.User.objects.filter(pk=idTeacher).first()
            document = models.Documentos(nombreDocumento=name,url_documento=directorio,idDocente=teacher)
            document.save()
            status="Archivo subido"
        except Exception as e:
            print e
            status="Error Archivo"
    else:
        status="Error Archivo"

    dictionary["status"] = status
    return JsonResponse(dictionary)


def get_documents_teacher(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    idTeacher=255468
    dataDocuments=[]
    teacher = models.User.objects.filter(pk=idTeacher).first()
    documents = models.Documentos.objects.filter(idDocente=teacher)
    for document in documents:
        document.append[document.pk, document.nombreDocumento, document.url_documento]
    dictionary["documents"] = dataDocuments
    return JsonResponse(dictionary)



def get_documents_teacher_web(request):
    idTeacher=request.POST.get("teacher")
    classroom=request.POST.get("classroom")
    dictionary={}
    url = 'static/Documents/'+str(classroom)+'/'+str(idTeacher)
    directorio = os.path.join(BASE_DIR,url)
    print directorio
    dataDocuments=[]
    teacher = models.User.objects.filter(pk=idTeacher).first()
    documents = models.Documentos.objects.filter(idDocente=teacher,url_documento=directorio)
    for document in documents:
        document.append[document.pk, document.nombreDocumento, document.url_documento]
    dictionary["documents"] = dataDocuments
    return JsonResponse(dictionary)


def delete_docuement(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode)
    idTeacher=255468
    idDocumento=1
    status = ""
    try:
        document = models.Documentos.objects.filter(idDocente=idTeacher , pk=idDocumento).first()
        document.delete()
        status = "OK"
    except Exception as e:
        status = "Error al eliminar documento"

    dictionary["status"] = status
    return JsonResponse(dictionary)
