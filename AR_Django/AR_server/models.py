# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models
from django_mysql.models import ListCharField
# Create your models here.

class datosUser(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    img_url = models.CharField(max_length=50)
    rol = models.ForeignKey('Roles')
    plan = models.CharField(max_length=15, default="SOF11")


class Roles(models.Model):
    rol = models.CharField(max_length=20)


class Docente(models.Model):
    cubiculo = models.CharField(max_length=30)
    extencion = models.CharField(max_length=15)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    informacionAdicional = models.CharField(max_length=500)

class Materia(models.Model):
    nombre = models.CharField(max_length=50)
    carrera = models.CharField(max_length=10)
    clave = models.CharField(max_length=20)
    semestre = models.CharField(max_length=20)
    descripcion = models.CharField(max_length=300)

class Aula(models.Model):
    nombre  = models.CharField(max_length=10)
    edicio = models.CharField(max_length=10)
    latitud = models.CharField(max_length=30)
    longitud = models.CharField(max_length=30)
    img_url = models.CharField(max_length=50)

class Horario(models.Model):
    horaInicio = models.CharField(max_length=45, blank=True, null=True)
    horaFinal = models.CharField(max_length=45, blank=True, null=True)
    docente = models.ForeignKey('Docente')
    materia = models.ForeignKey('Materia')
    aula = models.ForeignKey('Aula')
    dia = models.CharField(max_length=20)

class Comentario(models.Model):
    comentario = models.CharField(max_length=250)
    fecha = models.DateField()
    tipoComentario = models.CharField(max_length=50)
    estatus = models.CharField(max_length=20)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    id_clase_docente =  models.CharField(max_length=20)

class Calificacion(models.Model):
    calificacion = models.CharField(max_length=20)
    fecha = models.DateField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)

class Documentos(models.Model):
    idDocente =  models.ForeignKey(User, on_delete=models.CASCADE)
    nombreDocumento = models.CharField(max_length=100)
    url_documento = models.CharField(max_length=100)

class PuntoInteres(models.Model):
    latitud = models.CharField(max_length=30)
    longitud = models.CharField(max_length=30)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

class CampoInformativoContenido(models.Model):
    titulo = models.CharField(max_length=50)
    texto = models.CharField(max_length=255)
    fechaCreacion = models.DateField()
    puntoInteres = models.ForeignKey('PuntoInteres')
    img_url = models.CharField(max_length=50)

class MarcadoresCompartidos(models.Model):
    user_emisor = models.CharField(max_length=20)
    user_receptor = models.CharField(max_length=20)
    CIC =  models.ForeignKey('CampoInformativoContenido')

class CIC_CIL(models.Model):
    CIC = models.ForeignKey('CampoInformativoContenido')
    CIL = models.IntegerField()

class CIL_CIL(models.Model):
    CIL_primario = models.IntegerField()
    CIL_secundario = models.IntegerField()

class CampoInformativoLista(models.Model):
    puntoInteres = models.ForeignKey('PuntoInteres')
    titulo = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=255)


class SolicitudAmistad(models.Model):
    estatus = models.CharField(max_length=15, default="Pendiente")
    user_emisor = models.CharField(max_length=20)
    user_receptor = models.CharField(max_length=20)

class Amigos(models.Model):
    user_emisor = models.CharField(max_length=11)
    user_receptor = models.CharField(max_length=11)
    fecha = models.DateField()
